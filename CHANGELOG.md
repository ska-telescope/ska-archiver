All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[2.7.0]
**********
**1 ska-tango-archiver - 2.8.1 - 2.9.0:**
    
    a. Fixed SKB-445 by enabling the ingress for archwizard and archviewer tool.
    b. Fixed SKB-441 and SKB-440 by updating the archviewer version.
    c. Temporary disabling the multiple db and server support.

[2.5.1]
***********

**1 ska-tango-archiver - 2.8.0 - 2.8.1:**

    a. Corrected YAML syntax error with file globs.

**2 ska-tango-archiver-timescaledb - 0.1.5 - 0.1.6:**

    a.Updated timescaledb image version to pg16.4-ts2.16.1-all.

[2.5.0]
************
**Charts:**
**1. ska-tango-archiver - 2.7.1 - 2.8.0:**

    a. Update cpptango version to 9.5.0.

[2.4.14]
************
**Charts:**

**1. ska-tango-archiver - 2.7.0 - 2.7.1:**

* a. Removed unnecessary additional variable (configuration_manager,event_receiver) to resolved bug SKB-298
* b. Updated tango base chart v0.4.9 and tango util chart v0.4.10 
* c. Changed folder name from eda_configuration to ska_eda_configuration
* d. moved the ipynb files into notebooks folder to follow ska directory structure. 
    
**2. ska-tango-archiver-timescaledb - 0.1.4 - 0.1.5:**

* a. Remove the hardcoded path from the vault annotation.

[2.4.13]
************
**Charts:**

**1. ska-tango-archiver - 2.6.1 - 2.7.0:**

* a. Added auto configuration capability that is performed in an additional job.

**2. ska-tango-archiver-timescaledb - 0.1.4 - 0.1.5:**

* a. Remove the hardcoded path from the vault annotation.

