.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Engineering Data Archiver
==========================

.. toctree::
   :maxdepth: 2

   introduction/introduction
   deployment/index
   developer-guide/index


.. toctree::
   :maxdepth: 1
   :caption: Releases
   
   CHANGELOG.rst