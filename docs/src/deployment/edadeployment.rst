EDA Deployment
===============

The EDA deployment consist of deployment of Configuration Manager Tango Device, Event Subscriber Tango Device,Archwizard,
Archviewer and EDA configurator tool. 

| The Deployment requires TimescaleDB credentials along with some variables to connect with other tools in deployment.
| Below table shows mandatory values required by EDA chart while deployment.
| These values are present in charts/ska-tango-archiver/values.yaml or can be added in values.yaml.

**Note**

1. TimescaleDB credentials to be utilized using CI/CD variable. 
2. If the TimescaleDB chart is using vault in certain environment, please make sure to get the value manually 
   and update the latest value in CI/CD variable. 
3. Currently(6-10-2023) there is no provision to utilize vault in ska-tango-archiver chart.

.. list-table:: Values Required
   :widths: 15 50 15 15
   :header-rows: 1

   * - Key
     - description
     - type
     - default
   * - hostname
     - IP address or DNS of TimescaleDB server.
     - str
     - None
   * - dbname
     - Database name for archiving.Will be created if does not exists. 
     - str
     - default_archiver_db
   * - dbuser
     - Database user name
     - str
     - None
   * - port
     - Port of TimescaleDB server.
     - int
     - None
   * - dbpassword
     - Password of user specified.
     - str
     - None
   * - telescope
     - This variable helps to specify deployment type Low/Mid.
     - str
     - SKA-mid
   * - telescope_environment
     - This variable is used by EDA Configurator tool to display environment in which tool is deployed.
     - str
     - MID-STFC
   * - archwizard_config
     - This variable is required by archwizard tool for administration purpose.
     - str
     - "MyHDB=tango://tango-databaseds.ska-tango-archiver.svc.cluster.local:10000/mid-eda/cm/01"
   * - car_sources
     - This variable is required for auto configuration functionality. The user needs to provide a CAR source where the configuration file is present.
     - str
     - ""
   * - car_file_path
     - This variable is required for auto configuration functionality.The user needs to provide a CAR file path under source where the configuration file is present .
     - str
     - ""
   * - configuration_file_url
     - This variable is required for auto configuration functionality.The user needs to provide url of the configuration file.
     - str
     - ""


* Details of v2.9.0:

  * The support for multiple dabase and servers has been discontinued for sometime.
  * The variables used for the same are discontinued and updated in user guide.
  * Archwizard and archviewer are now accesible using the ingress url.
  * New helm variable "allowed_origins" has been introduced, that can be set to allow the origins to access the archviewer tool using ingress host.
  
  Default value: "k8s.stfc.skao.int"
  Example: 

.. code-block::

  ska-tango-archiver: #chart name
    allowed_origins:
      - host1
      - host2 

* Variables required in v2.7.0:

  * configuration_manager : Name of the configuration manager
  * event_subscriber: Name of event subscriber.
  * Note: This variables are now discarded in v2.7.1


* To deploy the archiver enter following command:

.. code-block::

    make k8s-install-chart
   
or can directly provide values to make command as shown below

.. code-block::

    make k8s-install-chart TELESCOPE=<SKA-low/mid> 
    ARCHIVER_DBNAME=<dbname> 
    ARCHIVER_TIMESCALE_HOST_NAME=<hostname>
    ARCHIVER_TIMESCALE_PORT=<port> 
    ARCHIVER_TIMESCALE_DB_USER=<dbuser>
    ARCHIVER_TIMESCALE_DB_PWD=<dbpassword>
    TELESCOPE_ENVIRONMENT=<environment name>
    ARCHWIZARD_CONFIG=<TRL (Tango Resource Locator)  of configuration Manager>

* The archiver gets deployed in namespace `ska-tango-archiver`

* On gitlab ci-cd Masked Environment variables are created `ARCHIVER_TIMESCALE_DB_PWD` , `ARCHIVER_TIMESCALE_DB_USER` , `ARCHIVER_TIMESCALE_HOST_NAME`, `ARCHIVER_TIMESCALE_PORT` which contains archiver database connection details.


* To delete the deployment enter following command:

.. code-block::

    make k8s-uninstall-chart