API
===


The REST based API's can be used to configure archiver using curl command or other http clients.

Following are the API's provided by EDA configurator tool with examples showing details required by it.

1. configure-archiver

      This API helps to configure archiver using yaml file and options["add_update"/"remove"].

      | http verb allowed : ``POST``
      | Content-Type: ``multipart/form-data``
      | header: ``accept:application/json``

      .. code-block::

             curl -X 'POST' \
            'http://<IP>:8003/configure-archiver' \
            -H 'accept: application/json' \
            -H 'Content-Type: multipart/form-data' \
            -F 'file=@<YAML FILE>;type=application/x-yaml' \
            -F 'option=add_update'

      .. code-block::

            curl -X 'POST' \
            'http://<IP>:8003/configure-archiver' \
            -H 'accept: application/json' \
            -H 'Content-Type: multipart/form-data' \
            -F 'file=@<YAML FILE>;type=application/x-yaml' \
            -F 'option=remove'

2. download-configuration

      The EventSubscriber Attributes API allows users to retrieve the current attributes of an event subscriber archived in the system. The event subscriber is identified by its name, which follows the format <mid-eda/es/01> or <low-eda/es/01>

      | http verb allowed : ``GET``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'GET' \
            'http://<IP>:8003/download-configuration/?eventsubscriber=mid-eda%2Fes%2F01' \
            -H 'accept: application/json'

3. [GET]configurations

      This API provides history of all the operations performed in the system.

      | http verb allowed : ``GET``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'GET' \
            'http://<IP>:8003/configurations' \
            -H 'accept: application/json'

4. [DELETE]configurations

      This API deletes history of operations performed.

      | http verb allowed : ``DELETE``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'DELETE' \
            'http://<IP>:8003/configurations' \
            -H 'accept: application/json'