Local Deployment and testing
=============================

    1. Need to connect to STFC VPN , if testing with STFC deployed TimescaleDB.
   
                                  OR
      
      Deploy TimescaleDB locally using following command:
        
      .. code-block::

        make timescaledeploy StorageClass=<VALUE>
        DATA_SIZE=<VALUE> WAL_SIZE=<VALUE> ADMIN_PASS=<VALUE> REPLICA_COUNT= <VALUE>


      .. code-block::
      
          StorageClass = Required for TimescaleDB Persistent Volume .
                        Mostly for local deployment it would be default.Default value is bds1. 
                        Set it as blank String ("")for local testing.
        
          DATA_SIZE =  Storage size for data storage
        
          WAL_SIZE = Storage size for WriteAheadLogs
        
          ADMIN_PASS = Admin password for TimescaleDB server
        
          REPLICA_COUNT = replica count of TimescaleDB server deployment
                                  

    2. Update database details in values.yaml file before deployment.

       currently host ip for mid :*10.200.10.143 or timescaledb.ska-eda-mid-db.svc.cluster.local* 

       currently host ip for low :*10.200.12.14 or timescaledb.ska-eda-low-db.svc.cluster.local*
      
      *NOTE:if not able to connect please check k8s-test in pipeline for ip address, might be due to redeployment of timescaledb.*

      and run ``make k8s-install-chart TELESCOPE=ska-low/mid``

                                              OR

    3. Deploy using folowing command, pass db details if different then the default one.
  
      .. code-block:: 

            make k8s-install-chart ARCHIVER_DBNAME=<dbname> ARCHIVER_TIMESCALE_HOST_NAME=10.200.10.143/timescaledb.ska-eda-mid-db.svc.cluster.local ARCHIVER_TIMESCALE_PORT=<port> ARCHIVER_TIMESCALE_DB_USER=<dbuser> ARCHIVER_TIMESCALE_DB_PWD=<dbpassword>
            
      for low deployment:
      
      .. code-block:: 

            make k8s-install-chart TELESCOPE=ska-low ARCHIVER_DBNAME=<dbname> ARCHIVER_TIMESCALE_HOST_NAME_LOW=10.200.12.14/timescaledb.ska-eda-low-db.svc.cluster.local ARCHIVER_TIMESCALE_PORT_LOW=<port> ARCHIVER_TIMESCALE_DB_USER_LOW=<dbuser> ARCHIVER_TIMESCALE_DB_PWD_LOW=<dbpassword>

    4. To test the deployment


      .. code-block:: 

            make k8s-test 

