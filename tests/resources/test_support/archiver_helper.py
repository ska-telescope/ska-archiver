"""Support for test archiver"""
import logging
from time import sleep

from tango import AttributeProxy, DeviceProxy


class ArchiverHelper:
    def __init__(self, conf_manager="mid-eda/cm/01", eventsubscriber="mid-eda/es/01"):
        self.conf_manager = conf_manager
        self.eventsubscriber = eventsubscriber
        self.conf_manager_proxy = DeviceProxy(self.conf_manager)
        self.evt_subscriber_proxy = DeviceProxy(self.eventsubscriber)

    def attribute_add(self, trl, polling_period=1000, period_event=3000):
        if self.is_already_archived(trl):
            logging.error("already archived attribute")
            return False
        elif polling_period > period_event:
            logging.error("polling period greater than period event")
            return False
        else:
            AttributeProxy(trl).read()
            self.conf_manager_proxy.write_attribute("SetAttributeName", trl)
            self.conf_manager_proxy.write_attribute("SetArchiver", self.eventsubscriber)
            self.conf_manager_proxy.write_attribute("SetStrategy", "ALWAYS")
            self.conf_manager_proxy.write_attribute("SetPollingPeriod", int(polling_period))
            self.conf_manager_proxy.write_attribute("SetPeriodEvent", int(period_event))
            self.conf_manager_proxy.AttributeAdd()
            return True

    def attribute_list(self):
        return self.evt_subscriber_proxy.AttributeList

    def is_already_archived(self, trl):
        attr_list = self.attribute_list()
        if attr_list is not None:
            for already_archived in attr_list:
                if trl in str(already_archived).lower():
                    return True
        return False

    def start_archiving(self, trl=None, polling_period=1000, period_event=3000):
        if trl is not None:
            self.attribute_add(trl, polling_period, period_event)
        return self.evt_subscriber_proxy.Start()

    def stop_archiving(self, trl):
        self.evt_subscriber_proxy.AttributeStop(trl)
        return self.conf_manager_proxy.AttributeRemove(trl)

    def evt_subscriber_attribute_status(self, trl):
        return self.evt_subscriber_proxy.AttributeStatus(trl)

    def conf_manager_attribute_status(self, trl):
        return self.conf_manager_proxy.AttributeStatus(trl)

    def is_started(self, trl):
        return "Archiving          : Started" in self.evt_subscriber_attribute_status(trl)

    def wait_for_start(self, trl, sleep_time=0.1, max_retries=30):
        total_sleep_time = 0
        for x in range(0, max_retries):
            try:
                if "Archiving          : Started" in self.conf_manager_attribute_status(trl):
                    break
            except Exception:
                pass
            sleep(sleep_time)
            total_sleep_time += 1
        return total_sleep_time * sleep_time
