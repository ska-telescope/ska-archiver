"""
This Module is used for testing EDA configurator API.
"""
import os

import httpx
import pytest
import yaml

namespace = os.getenv("KUBE_NAMESPACE")
cluster_domain = os.getenv("CLUSTER_DOMAIN")


def test_add_api(filename):
    """test function for adding api"""
    with open(f"/app/tests/resources/config_files/{filename}", "rb") as file:
        response = httpx.post(
            f"http://configurator.{namespace}.svc.{cluster_domain}:8003/configure-archiver",
            files={"file": (filename, file, "application/x-yaml")},
            data={"option": "add_update"},
        )
        response_data = response.json()
        assert len(response_data["add"]) == 2
    with open(
        f"/app/tests/resources/config_files/{filename}", "r+", encoding="utf-8"
    ) as arch_stream:
        data = yaml.load(arch_stream, Loader=yaml.Loader)
        for conf_data in data["configuration"]:
            attributes_configurations = conf_data.get("attributes")
            archived_attr = list(attributes_configurations.keys())

        add_list = [attribute_name.split("/")[-1] for attribute_name in response_data["add"]]
        for attribute in archived_attr:
            assert attribute in add_list


def test_remove_api(filename):
    """test function for removing api"""
    with open(f"/app/tests/resources/config_files/{filename}", "rb") as file:
        response = httpx.post(
            f"http://configurator.{namespace}.svc.{cluster_domain}:8003/configure-archiver",
            files={"file": (filename, file, "application/x-yaml")},
            data={"option": "remove"},
        )
        response_data = response.json()
        assert len(response_data["remove"]) == 2
    with open(
        f"/app/tests/resources/config_files/{filename}", "r+", encoding="utf-8"
    ) as arch_stream:
        data = yaml.load(arch_stream, Loader=yaml.Loader)
        for conf_data in data["configuration"]:
            attributes_configurations = conf_data.get("attributes")
            archived_attr = list(attributes_configurations.keys())

        remove_list = [attribute_name.split("/")[-1] for attribute_name in response_data["remove"]]
        for attribute in archived_attr:
            assert attribute in remove_list


def test_change_api(filename):
    """test function for changing api"""
    with open(f"/app/tests/resources/config_files/{filename}", "rb") as file:
        response = httpx.post(
            f"http://configurator.{namespace}.svc.{cluster_domain}:8003/configure-archiver",
            files={"file": (filename, file, "application/x-yaml")},
            data={"option": "add_update"},
        )
        response_data = response.json()
        assert len(response_data["change"]) == 1
    with open(
        f"/app/tests/resources/config_files/{filename}", "r+", encoding="utf-8"
    ) as arch_stream:
        data = yaml.load(arch_stream, Loader=yaml.Loader)
        for conf_data in data["configuration"]:
            attributes_configurations = conf_data.get("attributes")
            archived_attr = list(attributes_configurations.keys())

        change_list = [attribute_name.split("/")[-1] for attribute_name in response_data["change"]]
        for attribute in archived_attr:
            assert attribute in change_list


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_added_config_mid():
    """test case to add attribute for mid"""
    test_add_api("attribute_config.yaml")


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_added_config_low():
    """test case to add attribute for low"""
    test_add_api("attribute_config_low.yaml")


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_changed_config_mid():
    """test case to change attribute for mid"""
    test_change_api("change_attribute.yaml")


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_changed_config_low():
    """test case to change attribute for low"""
    test_change_api("change_attribute_low.yaml")


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_removed_config_mid():
    """test case to remove attribute for mid"""
    test_remove_api("removed_attribute.yaml")


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_removed_config_low():
    """test case to remove attribute for low"""
    test_remove_api("removed_attribute_low.yaml")
