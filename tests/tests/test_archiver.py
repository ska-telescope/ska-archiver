# -*- coding: utf-8 -*-
"""
Test archiver
"""
import logging
import sys
from time import sleep

import pytest
from tango import ApiUtil, DevFailed, DeviceProxy

from tests.resources.test_support.archiver_helper import ArchiverHelper


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_init_low():
    """Test case for archiver intialization for low"""
    logging.info("Init test archiver low")
    archiver_helper = ArchiverHelper(conf_manager="low-eda/cm/01", eventsubscriber="low-eda/es/01")
    archiver_helper.start_archiving()


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_init():
    """Test case for archiver intialization for mid"""
    logging.info("Init test archiver mid")
    archiver_helper = ArchiverHelper()
    archiver_helper.start_archiving()


def configure_attribute(attribute, configuration_manager, event_subscriber):
    """Method to initialize the archiver"""
    archiver_helper = ArchiverHelper(configuration_manager, event_subscriber)
    archiver_helper.attribute_add(attribute, 100, 300)
    archiver_helper.start_archiving()
    slept_for = archiver_helper.wait_for_start(attribute)
    logging.info("Slept for %ss before archiving started.", slept_for)
    assert "Archiving          : Started" in archiver_helper.conf_manager_attribute_status(
        attribute
    )
    assert "Archiving          : Started" in archiver_helper.evt_subscriber_attribute_status(
        attribute
    )
    archiver_helper.stop_archiving(attribute)


def test_configure_attribute(configuration_manager, event_subscriber):
    """To test attribute being configured"""
    attribute = "sys/tg_test/1/double_scalar"
    sleep_time = 20
    max_retries = 3
    total_slept = 0
    for num in range(0, max_retries):
        try:
            ApiUtil.cleanup()
            configure_attribute(attribute, configuration_manager, event_subscriber)
            break
        except DevFailed as devfailed:
            logging.info("configure_attribute exception: %s", sys.exc_info())
            try:
                device_adm = DeviceProxy("dserver/hdbppcm-srv/01")
                device_adm.RestartServer()
            except Exception:
                logging.info("reset_conf_manager exception: %s", sys.exc_info()[0])
            if num == (max_retries - 1):
                raise devfailed

        sleep(sleep_time)
        total_slept += 1

    if total_slept > 0:
        logging.info("Slept for %ss for the test configuration!", (total_slept * sleep_time))


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_config_attribute_mid():
    """Test configuration of attribute for mid"""
    try:
        test_configure_attribute("mid-eda/cm/01", "mid-eda/es/01")
    except Exception as error:
        logging.error(error)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_config_attribute_low():
    """Test configuration of attribute for low"""
    test_configure_attribute("low-eda/cm/01", "low-eda/es/01")
