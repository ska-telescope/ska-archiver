"""
This Module is used for testing auto configure functionality.
"""
import pytest

from tests.resources.test_support.archiver_helper import ArchiverHelper


@pytest.mark.auto_config
def test_auto_config():
    """Test auto configure functionality for Mid configuration"""
    archiver_helper = ArchiverHelper()
    assert archiver_helper.is_already_archived("sys/tg_test/1/short_scalar_ro")
    assert archiver_helper.is_already_archived("sys/tg_test/1/double_image")


@pytest.mark.auto_config_low
def test_auto_config_low():
    """Test auto configure functionality for Low configuration"""
    archiver_helper = ArchiverHelper(conf_manager="low-eda/cm/01", eventsubscriber="low-eda/es/01")
    assert archiver_helper.is_already_archived("sys/tg_test/1/short_scalar_ro")
    assert archiver_helper.is_already_archived("sys/tg_test/1/double_image")
