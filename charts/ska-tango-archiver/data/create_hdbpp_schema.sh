#!/bin/bash

app_path=$(realpath app)
timescale_script_path="${app_path}/data/hdb_schema.sql"
echo $timescale_script_path

# Provide archiver database name as a comand line argument
echo "Creating the archiver database and HDB++ Schema ...."
echo "SELECT 'CREATE DATABASE $1' WHERE NOT EXISTS(SELECT FROM pg_database WHERE datname = '$1')\gexec" | psql -h $2 -p $3 -U $4 -d postgres
psql -h $2 -p $3 -U $4 -d $1  -c "\c $1;" -a -f "$timescale_script_path"

echo "HDB++ Schema is created."
