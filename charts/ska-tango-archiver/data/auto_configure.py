""" Python script to configure EDA using file link provided
"""
import argparse
import json
import logging
import os
import time
from typing import Tuple

import requests
from ska_ser_logging import configure_logging
from ska_telmodel.data import TMData

configure_logging()
logger = logging.getLogger("auto_configure")


def create_file() -> Tuple[str, str]:
    """Method used to create file in the auto_config_files folder.
    Returns:
        Tuple(str,str): Returns file name and file path
    """
    file_name = f"auto_config_{time.time()}.yaml"
    folder_path = os.path.join(os.getcwd(), "auto_config_files")
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    filepath = os.path.join(folder_path, file_name)
    return file_name, filepath


def save_downloaded_file_in_file_path(filepath: str, configuration_file: str) -> None:
    """Method used to write the content of the downloaded file
       in the file mentioned in the argument.

    Args:
        file_path (str): file path where downloaded content needs to be updated
        config_file (str): downloaded file
    """
    logger.info(configuration_file)
    with open(filepath, "wb") as serverfile:
        serverfile.write(configuration_file)


def configure_archiver(filepath: str, arguments: argparse.Namespace) -> requests.Response:
    """Method uses yaml file to configure the archiver using configure-archiver API.

    Args:
        file_path (str): file path to upload for configuring the archiver

    Returns:
        requests.Response: Response class object with details related to response from the server.
    """
    with open(filepath, "rb") as file:
        response_data = requests.post(
            f"http://configurator.{arguments.namespace}.svc.{arguments.cluster_domain}"
            + ":8003/configure-archiver",
            files={"file": (filename, file, "application/x-yaml")},
            data={"option": "add_update"},
            timeout=50,
        )
    return response_data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "configuration_file_url",
        type=str,
        help="A link to YAML file containing archiver configuration.",
    )
    parser.add_argument(
        "car_sources",
        type=str,
        help="car source where configuration file is stored",
    )
    parser.add_argument(
        "car_file_path",
        type=str,
        help="file path under source where configuration file is present.",
    )
    parser.add_argument(
        "namespace",
        type=str,
        help="Namespace of the configurator tool.",
    )
    parser.add_argument(
        "cluster_domain",
        type=str,
        help="cluster domain of the configurator.",
    )
    parser.add_argument(
        "tango_host",
        type=str,
        help="tango host of the tango database.",
    )
    args = parser.parse_args()
    CONFIG_FILE = None
    ERROR_MSG: str = ""
    try:
        if args.car_sources and args.car_file_path:
            logger.info("Using car link %s,%s", args.car_sources, args.car_file_path)
            CONFIG_FILE = TMData([args.car_sources])[args.car_file_path].get()
        elif args.configuration_file_url:
            response = requests.get(args.configuration_file_url, timeout=50)
            CONFIG_FILE = response.content
        else:

            logger.info(
                "Please update (car_sources,car_file_path)"
                + "or configuration_file_url correctly.\n"
                + "current value of car_sources:%s\n"
                + "current value of car_file_path:%s\n"
                + "current value of configuration_file_url:%s",
                args.car_sources,
                args.car_file_path,
                args.configuration_file_url,
            )

        if CONFIG_FILE:
            filename, file_path = create_file()
            save_downloaded_file_in_file_path(file_path, CONFIG_FILE)
            response = configure_archiver(file_path, args)
            logger.info(
                "Response from the server,status: %s and " + " message: %s",
                response.status_code,
                response.content,
            )
            response_content: dict = json.loads(response.content)
            if response_content.get("partial_error"):
                ERROR_MSG = "Error occurred while archiving some attributes\n"
                for log in response_content["partial_error"]:
                    ERROR_MSG += log + "\n"
                logger.error(ERROR_MSG)
            elif response_content.get("error"):
                ERROR_MSG = "Error occurred during archiving:\n"
                for log in response_content["error"]:
                    ERROR_MSG += log + "\n"
                logger.error(ERROR_MSG)

    except Exception as exception:
        logger.exception("Exception occurred :%s", exception)
